phrase(<u, v>, affirmation(l)) -> ph_aff(<u, ".".v>, l);
phrase(<u, v>, l) -> ph_int(<u, "?".v>, l);

ph_aff(<u, v>, l) -> sujet(<u, w>, g, x) gv(<w, v>, <g, n>, <x, l>);

ph_int(<"est"."-"."ce"."que".u, v>, est_ce_que(l)) -> ph_aff(<u, v>, l);
ph_int(<"qui".u, v>, quel(x, l)) -> gv(<u, v>, <g, n>, <x, l>);
ph_int(<u, v>, est_ce_que(l)) -> sujet(<u, w>, g, x) gv_til(<w, v>, <g, n>, <x, l>);
ph_int(<"pourquoi".u, v>, pourquoi(l)) -> sujet(<u, w>, g, x) gv_til(<w, v>, <g, n>, <x, l>);
ph_int(<"de"."qui".u, v>, quel(l)) -> sujet(<u, w>, g, x) gv_til_partiel(<w, v>, <g, n>, <x, l>);
ph_int(<"que"."sais"."-"."tu".u, u>, que_sais_tu.nil) ->;

sujet(<u, v>, g, x) -> nom_pr(<u, v>, g, x);

gv(<u, v>, <g, n>, <x, l>) -> verbe(<u, w>, n) gn(<w, v>, <g, n>, <x, l>);

gv_til(<u, v>, <g, n>, <x, l>) -> verbe(<u, "-".w>, n) pronom(<w, z>, <g, n>) gn(<z, v>, <g1, n1>, <x, l>);

gv_til_partiel(<u, v>, <g, n>, <y, p>) -> verbe(<u, "-".w>, n) pronom(<w, x>, <g, n>) gn_partiel(<x, v>, <g1, n1>, <y, p>);

verbe(<"est".u, u>, si) ->;
verbe(<"sont".u, u>, pl) ->;

gn(<u, v>, <g, n>, <x, nil>) -> nom_pr(<u, v>, g, x);
gn(<u, v>, <g, n>, <x, p.nil>) -> art_ind(<u, w>, <g, n>) nom_com1(<w, v>, <g, n>, <p, x>);
gn(<u, v>, <g, n>, <x, p.l>) -> art_def(<u, w>, <g, n>) nom_com2(<w, z>, <g, n>, <p, x, y>) gp(<z, v>, <y, l>);

gn_partiel(<u, v>, <g, n>, <x, p>) -> art_def(<u, w>, <g, n>) nom_com2(<w, v>, <g, n>, <p, x, y>);

art_def(<"le".u, u>, <ma, si>) ->;
art_def(<"la".u, u>, <fe, si>) ->;
art_def(<"les".u, u>, <ma, pl>) ->;
art_def(<"les".u, u>, <fe, pl>) ->;

art_ind(<"un".u, u>, <ma, si>) ->;
art_ind(<"une".u, u>, <fe, si>) ->;
art_ind(<"des".u, u>, <ma, pl>) ->;
art_ind(<"des".u, u>, <fe, pl>) ->;

gp(<"de".u, v>, <x, l>) -> gn(<u, v>, <g, n>, <x, l>);

pronom(<"il".u, u>, <ma, si>) ->;
pronom(<"ils".u, u>, <ma, pl>) ->;
pronom(<"elle".u, u>, <fe, si>) ->;
pronom(<"elles".u, u>, <fe, pl>) ->;

nom_com1(<"homme".u, u>, <ma, si>, <homme(x), x>) ->;
nom_com1(<"hommes".u, u>, <ma, pl>, <homme(x), x>) ->;
nom_com1(<"femme".u, u>, <fe, si>, <femme(x), x>) ->;
nom_com1(<"femmes".u, u>, <fe, pl>, <femme(x), x>) ->;

nom_com2(<"pere".u, u>, <ma, si>, <pere(x, y), x, y>) ->;
nom_com2(<"mere".u, u>, <fe, si>, <mere(x, y), x, y>) ->;
nom_com2(<"frere".u, u>, <ma, si>, <frere(x, y), x, y>) ->;
nom_com2(<"freres".u, u>, <ma, pl>, <frere(x, y), x, y>) ->;
nom_com2(<"soeur".u, u>, <fe, si>, <soeur(x, y), x, y>) ->;
nom_com2(<"soeurs".u, u>, <fe, pl>, <soeur(x, y), x, y>) ->;
nom_com2(<"fils".u, u>, <ma, si>, <fils(x, y), x, y>) ->;
nom_com2(<"fils".u, u>, <ma, pl>, <fils(x, y), x, y>) ->;
nom_com2(<"fille".u, u>, <fe, si>, <fille(x, y), x, y>) ->;
nom_com2(<"filles".u, u>, <fe, pl>, <fille(x, y), x, y>) ->;
nom_com2(<"oncle".u, u>, <ma, si>, <oncle(x, y), x, y>) ->;
nom_com2(<"oncles".u, u>, <ma, pl>, <oncle(x, y), x, y>) ->;
nom_com2(<"tante".u, u>, <fe, si>, <tante(x, y), x, y>) ->;
nom_com2(<"tantes".u, u>, <fe, pl>, <tante(x, y), x, y>) ->;
nom_com2(<"cousin".u, u>, <ma, si>, <cousin(x, y), x, y>) ->;
nom_com2(<"cousine".u, u>, <fe, si>, <cousine(x, y), x, y>) ->;
nom_com2(<"cousins".u, u>, <ma, pl>, <cousin(x, y), x, y>) ->;
nom_com2(<"cousines".u, u>, <fe, pl>, <cousine(x, y), x, y>) ->;
nom_com2(<"grand"."-"."pere".u, u>, <ma, si>, <grand_pere(x, y), x, y>) ->;
nom_com2(<"grand"."-"."mere".u, u>, <fe, si>, <grand_mere(x, y), x, y>) ->;
nom_com2(<"grand"."-"."peres".u, u>, <ma, pl>, <grand_pere(x, y), x, y>) ->;
nom_com2(<"grand"."-"."meres".u, u>, <fe, pl>, <grand_mere(x, y), x, y>) ->;
nom_com2(<"petit"."-"."fils".u, u>, <ma, si>, <petit_fils(x, y), x, y>) ->;
nom_com2(<"petite"."-"."fille".u, u>, <fe, si>, <petite_fille(x, y), x, y>) ->;
nom_com2(<"petits"."-"."fils".u, u>, <ma, pl>, <petite_fils(x, y), x, y>) ->;
nom_com2(<"petites"."-"."filles".u, u>, <fe, pl>, <petite_fille(x, y), x, y>) ->;

nom_pr(<"Marie".u, u>, fe, Marie) ->;
nom_pr(<"Nina".u, u>, fe, Nina) ->;
nom_pr(<"Lea".u, u>, fe, Lea) ->;
nom_pr(<"Jade".u, u>, fe, Jade) ->;
nom_pr(<"Manon".u, u>, fe, Manon) ->;
nom_pr(<"Paul".u, u>, ma, Paul) ->;
nom_pr(<"Jean".u, u>, ma, Jean) ->;
nom_pr(<"Luc".u, u>, ma, Luc) ->;
nom_pr(<"Tom".u, u>, ma, Tom) ->;
nom_pr(<"Hugo".u, u>, ma, Hugo) ->;

/*TRAITEMENT*/
traiter(affirmation(t.q)) -> assert(t, q);
traiter(quel(x, s)) -> liste_solutions(x, s, l) outl(l);
traiter(est_ce_que(s)) -> liste_solutions(x, s, l) outl(l);

liste_solutions(x, m, l) -> list_of(x, nil, m, l);

hors(x, nil) ->;
hors(x, e.l) -> dif(x, e) hors(x, l);

noloop(nil, l) ->;
noloop(dif(x, y).m, l) -> ! dif(x, y) noloop(m, l);
noloop(p.m, l) -> hors(p, l) rule(p, q) noloop(q, p.l) noloop(m, l);

out_sentence(nil) -> outm("\b");
out_sentence("-".l) -> ! outm("\b-") out_sentence(l);
out_sentence("l"."'".l) -> ! outm("l'") out_sentence(l);
out_sentence(x.l) -> outm(x) outm(" ") out_sentence(l);

/*MOTEUR*/
go -> line outm("A vous : ") in_sentence(s, t) suite(s);
suite("au"."revoir".".".nil) -> ! outml("C'etait un plaisir. Au revoir !");
/*suite(s) -> forme_usuelle(<s, v>) phrase(<v, nil>, l) ! outm("La phrase est correcte. J'obtiens la structure : ") outl(l) go;*/
suite(s) -> forme_usuelle(<s, v>) phrase(<v, nil>, l) ! traiter(l) go;
suite(s) -> outml("Desole, je ne comprends pas.") go;

/*FORME USUELLE*/
forme_usuelle(<nil, nil>) ->;
forme_usuelle(<"du".u, "de"."le".v>) -> ! forme_usuelle(<u, v>);
forme_usuelle(<"des".u, "de"."les".v>) -> ! forme_usuelle(<u, v>);
forme_usuelle(<"l"."'"."oncle".u, "le"."oncle".v>) -> ! forme_usuelle(<u, v>);
/*forme_usuelle(<"d"."'".u, "de".v>) -> ! forme_usuelle(<u, v>);*/
forme_usuelle(<x.u, x.v>) -> forme_usuelle(<u, v>);

/*META CONNAISSANCES*/
pere(x, y) -> homme(x) noloop(fils(y, x).nil, nil);
pere(x, y) -> homme(x) noloop(fille(y, x).nil, nil);
mere(x, y) -> femme(x) noloop(fils(y, x).nil, nil);
mere(x, y) -> femme(x) noloop(fille(y, x).nil, nil);
fils(x, y) -> homme(x) noloop(pere(y, x).nil, nil);
fils(x, y) -> homme(x) noloop(mere(y, x).nil, nil);
fille(x, y) -> femme(x) noloop(mere(y, x).nil, nil);
fille(x, y) -> femme(x) noloop(pere(y, x).nil, nil);
frere(x, y) -> noloop(pere(x, p).mere(x, m).pere(y, p).mere(y, m).nil, nil);
soeur(x, y) -> noloop(pere(x, p).mere(x, m).pere(y, p).mere(y, m).nil, nil);
oncle(x, y) -> homme(x) noloop(frere(x, f).pere(f, y).nil, nil);
oncle(x, y) -> homme(x) noloop(frere(x, f).mere(f, y).nil, nil);
tante(x, y) -> femme(x) noloop(soeur(x, f).mere(f, y).nil, nil);
tante(x, y) -> femme(x) noloop(soeur(x, f).pere(f, y).nil, nil);
/*cousins grand parent petits enfants*/

femme(x) -> nom_pr(<n.nil, nil>, fe, x);
homme(x) -> nom_pr(<n.nil, nil>, ma, x);
